package gui;

/**
 * @author Software Engineering teachers
 */

import javax.swing.*;

import domain.Event;
import domain.Kuota;
import domain.Question;
import businessLogic.BLFacade;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SetResultGUI extends JFrame {

	public static SetResultGUI setresultgui;

	private Question q;
	
	private String selectedEmaitza;

	private static final long serialVersionUID = 1L;

	private JPanel jContentPane = null;

	private static BLFacade appFacadeInterface;

	public static BLFacade getBusinessLogic() {
		return appFacadeInterface;
	}

	public static void setBussinessLogic(BLFacade afi) {
		appFacadeInterface = afi;
	}

	protected JLabel jLabelJarriKuota;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextArea textArea = new JTextArea();
	private JComboBox<String> comboBoxEmaitzak = new JComboBox();
	private DefaultComboBoxModel<String> emaitzakInfo = new DefaultComboBoxModel<String>();
	private JButton btnEmaitzaIpini = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("IpiniEmaitza"));

	/**
	 * This is the default constructor
	 */
	public SetResultGUI(Question ques) {
		super();

		setresultgui = this;
		this.q = ques;

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					// if (ConfigXML.getInstance().isBusinessLogicLocal()) facade.close();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.out.println(
							"Error: " + e1.toString() + " , probably problems with Business Logic or Database");
				}
				System.exit(1);
			}
		});

		initialize();
		// this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		// this.setSize(271, 295);
		this.setSize(495, 290);
		this.setContentPane(getJContentPane());
		this.setTitle(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("MainTitle"));
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);

			JLabel jLabelAukeratuEmaitza = new JLabel(
					ResourceBundle.getBundle(MainGUI.hizkuntza).getString("AukeratuEmaitza"));
			jLabelAukeratuEmaitza.setHorizontalAlignment(SwingConstants.CENTER);
			jLabelAukeratuEmaitza.setForeground(Color.BLACK);
			jLabelAukeratuEmaitza.setFont(new Font("Tahoma", Font.BOLD, 11));
			jLabelAukeratuEmaitza.setBounds(146, 11, 179, 41);
			jContentPane.add(jLabelAukeratuEmaitza);

			JButton btnItzuli = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli"));
			btnItzuli.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setresultgui.setVisible(false);
					dispose();
				}
			});
			btnItzuli.setBounds(137, 202, 188, 23);
			jContentPane.add(btnItzuli);

			textArea.setEditable(false);
			textArea.setBounds(56, 94, 359, 58);
			jContentPane.add(textArea);
			comboBoxEmaitzak.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					selectedEmaitza = (String) comboBoxEmaitzak.getSelectedItem();
					btnEmaitzaIpini.setEnabled(true);
				}
			});

			comboBoxEmaitzak.setBounds(56, 52, 359, 20);
			jContentPane.add(comboBoxEmaitzak);
			for (Kuota k : q.getKuotak())
				emaitzakInfo.addElement(k.getErantzunP());
			comboBoxEmaitzak.setModel(emaitzakInfo);
			btnEmaitzaIpini.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					BLFacade facade = MainGUI.getBusinessLogic();
					if(facade.emaitzaIpini(q, selectedEmaitza))
						q.setResult(selectedEmaitza);
					else
						textArea.setText("Saiatu berriro");
				}
			});
			btnEmaitzaIpini.setBounds(137, 168, 188, 23);
			
			jContentPane.add(btnEmaitzaIpini);
			btnEmaitzaIpini.setEnabled(false);
		}
		return jContentPane;

	}

} // @jve:decl-index=0:visual-constraint="0,0"
