package gui;

/**
 * @author Software Engineering teachers
 */


import javax.swing.*;

import domain.Event;
import domain.Question;
import businessLogic.BLFacade;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class CreateKuotaGUI extends JFrame {
	
	public static CreateKuotaGUI crkuotagui;
	
	private Question q;
	
	private static final long serialVersionUID = 1L;

	private JPanel jContentPane = null;

    private static BLFacade appFacadeInterface;
	
	public static BLFacade getBusinessLogic(){
		return appFacadeInterface;
	}
	 
	public static void setBussinessLogic (BLFacade afi){
		appFacadeInterface=afi;
	}
	protected JLabel jLabelJarriKuota;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnKuotaSortu;
	private JTextField textFieldKuota;
	private JTextField textFieldEmaitza;
	private JTextArea textArea = new JTextArea();
	
	/**
	 * This is the default constructor
	 */
	public CreateKuotaGUI(Question ques) {
		super();
		
		crkuotagui=this;
		this.q=ques;
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					//if (ConfigXML.getInstance().isBusinessLogicLocal()) facade.close();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.out.println("Error: "+e1.toString()+" , probably problems with Business Logic or Database");
				}
				System.exit(1);
			}
		});

		initialize();
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		// this.setSize(271, 295);
		this.setSize(495, 290);
		this.setContentPane(getJContentPane());
		this.setTitle(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("MainTitle"));
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			
			JComboBox comboBox = new JComboBox();
			comboBox.setBounds(365, 1, 114, 20);
			jContentPane.add(comboBox);
			jContentPane.add(getBtnKuotaSortu());
			
			textFieldKuota = new JTextField();
			textFieldKuota.setBounds(30, 63, 169, 20);
			jContentPane.add(textFieldKuota);
			textFieldKuota.setColumns(10);
			
			JLabel jLabelJarriEmaitza = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("JarriEmaitza")); //$NON-NLS-1$ //$NON-NLS-2$
			jLabelJarriEmaitza.setHorizontalAlignment(SwingConstants.CENTER);
			jLabelJarriEmaitza.setForeground(Color.BLACK);
			jLabelJarriEmaitza.setFont(new Font("Tahoma", Font.BOLD, 11));
			jLabelJarriEmaitza.setBounds(265, 22, 179, 41);
			jContentPane.add(jLabelJarriEmaitza);
			
			textFieldEmaitza = new JTextField();
			textFieldEmaitza.setColumns(10);
			textFieldEmaitza.setBounds(275, 63, 169, 20);
			jContentPane.add(textFieldEmaitza);
			
			JButton btnItzuli = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli")); //$NON-NLS-1$ //$NON-NLS-2$
			btnItzuli.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					crkuotagui.setVisible(false);
					dispose();
				}
			});
			btnItzuli.setBounds(145, 202, 188, 23);
			jContentPane.add(btnItzuli);
			
			textArea.setEditable(false);
			textArea.setBounds(56, 94, 359, 58);
			jContentPane.add(textArea);
			
			JLabel labelJarriKuota = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("JarriKuota")); //$NON-NLS-1$ //$NON-NLS-2$
			labelJarriKuota.setHorizontalAlignment(SwingConstants.CENTER);
			labelJarriKuota.setForeground(Color.BLACK);
			labelJarriKuota.setFont(new Font("Tahoma", Font.BOLD, 11));
			labelJarriKuota.setBounds(20, 22, 179, 41);
			jContentPane.add(labelJarriKuota);
		}
		return jContentPane;
	}
	
	private JButton getBtnKuotaSortu() {
		if (btnKuotaSortu == null) {
			btnKuotaSortu = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("kuotaSortu")); //$NON-NLS-1$ //$NON-NLS-2$
			btnKuotaSortu.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String em= textFieldEmaitza.getText();
					if(!(textFieldKuota.getText().isEmpty() || em.isEmpty())) {
						if(Integer.parseInt(textFieldKuota.getText())>0) {
							
							try {
								float k= Float.parseFloat(textFieldKuota.getText());
								BLFacade facade = MainGUI.getBusinessLogic();
								int egoera= facade.createKuota(q, k, em);
								if(egoera==1)
									textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("KuotExistitzenDa"));
								if(egoera==2)
									textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("SahiatuBerriro"));
								if(egoera==0)
									textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("OndoSortu"));
									q.addNewKuota(k, em);
								
							}
							catch(java.lang.NumberFormatException er) {
								textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("KuotanZenbakiaSartu"));
							}
						}else {
							textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Positiboa"));
						}
					}else {
						textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("BeharrezkoInfoSartu"));
					}
				}
			});
			btnKuotaSortu.setBounds(145, 163, 188, 23);
		}
		return btnKuotaSortu;
	}
} // @jve:decl-index=0:visual-constraint="0,0"

