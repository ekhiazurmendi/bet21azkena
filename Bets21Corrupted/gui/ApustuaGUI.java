package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import businessLogic.BLFacade;
import domain.Bezeroa;
import domain.Kuota;
import domain.Question;

import javax.swing.*;

public class ApustuaGUI extends JFrame {

	public static ApustuaGUI apustuagui;
	private JPanel contentPane;
	private JTextField textField;
	private Kuota k;
	private JLabel lblApustuaLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Apustua"));
	private JButton btnApustuaEginButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ApostuaEgin"));
	private JButton btnNewButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli"));
	private JLabel labelError = new JLabel();

	/*
	 * /** Launch the application.
	 * 
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { ApustuaGUI frame = new ApustuaGUI();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */
	/**
	 * Create the frame.
	 */
	public ApustuaGUI(Kuota kuota) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 281, 212);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.k=kuota;
		apustuagui=this;
		BLFacade facade = MainGUI.getBusinessLogic();
		
		lblApustuaLabel.setBounds(32, 44, 63, 14);
		contentPane.add(lblApustuaLabel);
		
		
		labelError.setBounds(32, 56, 223, 14);
		contentPane.add(labelError);

		textField = new JTextField();
		textField.setBounds(92, 41, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		contentPane.add(btnNewButton);
		
		btnApustuaEginButton.setBounds(32, 78, 159, 23);
		contentPane.add(btnApustuaEginButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnNewButton.setBounds(32, 112, 159, 23);
		
		JLabel lblDiruaLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Dirua")+": "+((Bezeroa)(LoginGUI.logeatua)).getDirua()); //$NON-NLS-1$ //$NON-NLS-2$
		lblDiruaLabel.setBounds(32, 11, 146, 14);
		contentPane.add(lblDiruaLabel);
		btnApustuaEginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				labelError.setText("");
				String a = textField.getText();
				Date data = new Date();
				Bezeroa b = (Bezeroa) LoginGUI.logeatua; 
				if(!textField.getText().isEmpty()) {
					if(Float.parseFloat(a)>0) {
						if(b.getDirua()>=Float.parseFloat(a)) {
							if(k.getGaldera().getBetMinimum()<=Float.parseFloat(a)) {
								try {
										b=facade.createApustua(Float.parseFloat(a), data, b, k);
										if(b!=null) {
											LoginGUI.logeatua=b;
										}else {
											b=(Bezeroa)LoginGUI.logeatua;
										}
										lblDiruaLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Dirua")+": "+((Bezeroa)(LoginGUI.logeatua)).getDirua());
										//k.addNewApustua(Float.parseFloat(a), data, b, k);
										//b.addNewApustua(Float.parseFloat(a), data, b, k);
									
								}catch (NumberFormatException e) {
									labelError.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("SartuZenbakiBat"));
								}
							}else {
								labelError.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("InporteMinimoa"));
							}
						}else {
							labelError.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DiruGutxi"));
						}
					}else {
						labelError.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Positiboa"));
					}
				}else {
					labelError.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("BeharrezkoInfoSartu"));
				}

			}
		});
	}
}
