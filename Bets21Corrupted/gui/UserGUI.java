package gui;

/**
 * @author Software Engineering teachers
 */


import javax.swing.*;

import domain.Event;
import businessLogic.BLFacade;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class UserGUI extends JFrame {
	
	public static UserGUI usergui;
	
	private static final long serialVersionUID = 1L;

	private JPanel jContentPane = null;

    private static BLFacade appFacadeInterface;
	
	public static BLFacade getBusinessLogic(){
		return appFacadeInterface;
	}
	 
	public static void setBussinessLogic (BLFacade afi){
		appFacadeInterface=afi;
	}
	protected JLabel jLabelSelectOption;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnSaioaItxi;
	private JButton DiruaSartuButton;
	private JButton MugimenduakButton;
	private JButton btnNireApustuakButton;
	
	/**
	 * This is the default constructor
	 */
	public UserGUI() {
		super();
		
		usergui=this;
		setBounds(100, 100, 450, 400);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					//if (ConfigXML.getInstance().isBusinessLogicLocal()) facade.close();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.out.println("Error: "+e1.toString()+" , probably problems with Business Logic or Database");
				}
				System.exit(1);
			}
		});

		initialize();
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		// this.setSize(271, 295);
		this.setSize(495, 358);
		this.setContentPane(getJContentPane());
		this.setTitle(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("MainTitle"));
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getLblNewLabel());
			
			JButton jButtonQueryQueries_1 = new JButton();
			jButtonQueryQueries_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					usergui.setVisible(false);
					FindQuestionsGUIBezero a= new FindQuestionsGUIBezero(2);
					a.setVisible(true);
				}
			});
			jButtonQueryQueries_1.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("QueryQuestions"));
			jButtonQueryQueries_1.setBounds(25, 48, 428, 49);
			jContentPane.add(jButtonQueryQueries_1);
			jContentPane.add(getBtnSaioaItxi());
			jContentPane.add(getDiruaSartuButton());
			jContentPane.add(getMugimenduakButton());
			jContentPane.add(getBtnNireApustuakButton());
			
			JButton MezuButton = new JButton("");
			MezuButton.setBounds(390, 0, 89, 23);
			jContentPane.add(MezuButton);
			
			
		}
		return jContentPane;
	}
	

	private JLabel getLblNewLabel() {
		if (jLabelSelectOption == null) {
			jLabelSelectOption = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("SelectOption"));
			jLabelSelectOption.setBounds(0, 0, 469, 23);
			jLabelSelectOption.setFont(new Font("Tahoma", Font.BOLD, 13));
			jLabelSelectOption.setForeground(Color.BLACK);
			jLabelSelectOption.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jLabelSelectOption;
	}
	
	private void redibujar() {
		jLabelSelectOption.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("SelectOption"));
		this.setTitle(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("MainTitle"));
	}
	private JButton getBtnSaioaItxi() {
		if (btnSaioaItxi == null) {
			btnSaioaItxi = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("saioaItxi")); //$NON-NLS-1$ //$NON-NLS-2$
			btnSaioaItxi.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					LoginGUI.logeatua=null;
					//usergui.setVisible(false);
					LoginGUI.loginGui.setVisible(true);
					dispose();
				}
			});
			btnSaioaItxi.setBounds(145, 285, 188, 23);
		}
		return btnSaioaItxi;
	}
	private JButton getDiruaSartuButton() {
		if (DiruaSartuButton == null) {
			DiruaSartuButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DiruaSartu"));
			DiruaSartuButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					usergui.setVisible(false);
					JFrame a = new DiruaSartuGUI();
					a.setVisible(true);
				}
			});
			DiruaSartuButton.setBounds(25, 225, 428, 49);
		}
		return DiruaSartuButton;
	}
	private JButton getMugimenduakButton() {
		if (MugimenduakButton == null) {
			MugimenduakButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("MugimenduakIkusi"));
			MugimenduakButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					usergui.setVisible(false);
					JFrame a = new MugimenduGUI();
					a.setVisible(true);
				}
			});
			MugimenduakButton.setBounds(25, 165, 428, 49);
		}
		return MugimenduakButton;
	}
	
	private JButton getBtnNireApustuakButton() {
		if (btnNireApustuakButton == null) {
			btnNireApustuakButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NireApustuak")); //$NON-NLS-1$ //$NON-NLS-2$
			btnNireApustuakButton.setBounds(25, 108, 428, 46);
			btnNireApustuakButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					usergui.setVisible(false);
					BezeroApustuakGUI a= new BezeroApustuakGUI();
					a.setVisible(true);
				}
			});
			
		}
		return btnNireApustuakButton;
	}
} // @jve:decl-index=0:visual-constraint="0,0"

