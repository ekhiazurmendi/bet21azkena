package dataAccess;

//hello
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import configuration.ConfigXML;
import configuration.UtilDate;
import exceptions.QuestionAlreadyExist;

import domain.*;

/**
 * It implements the data access to the objectDb database
 */
public class DataAccess {
	protected static EntityManager db;
	protected static EntityManagerFactory emf;

	ConfigXML c = ConfigXML.getInstance();

	public DataAccess(boolean initializeMode) {

		System.out.println("Creating DataAccess instance => isDatabaseLocal: " + c.isDatabaseLocal()
				+ " getDatabBaseOpenMode: " + c.getDataBaseOpenMode());

		open(initializeMode);

	}

	public DataAccess() {
		this(false);
	}

	/**
	 * This is the data access method that initializes the database with some events
	 * and questions. This method is invoked by the business logic (constructor of
	 * BLFacadeImplementation) when the option "initialize" is declared in the tag
	 * dataBaseOpenMode of resources/config.xml file
	 */
	public void initializeDB() {

		db.getTransaction().begin();
		try {

			Calendar today = Calendar.getInstance();

			int month = today.get(Calendar.MONTH);
			month += 1;
			int year = today.get(Calendar.YEAR);
			if (month == 12) {
				month = 0;
				year += 1;
			}

			Event ev1 = new Event(1, "Atlético-Athletic", UtilDate.newDate(year, month, 17));
			Event ev2 = new Event(2, "Eibar-Barcelona", UtilDate.newDate(year, month, 17));
			Event ev3 = new Event(3, "Getafe-Celta", UtilDate.newDate(year, month, 17));
			Event ev4 = new Event(4, "Alavés-Deportivo", UtilDate.newDate(year, month, 17));
			Event ev5 = new Event(5, "Español-Villareal", UtilDate.newDate(year, month, 17));
			Event ev6 = new Event(6, "Las Palmas-Sevilla", UtilDate.newDate(year, month, 17));
			Event ev7 = new Event(7, "Malaga-Valencia", UtilDate.newDate(year, month, 17));
			Event ev8 = new Event(8, "Girona-Leganés", UtilDate.newDate(year, month, 17));
			Event ev9 = new Event(9, "Real Sociedad-Levante", UtilDate.newDate(year, month, 17));
			Event ev10 = new Event(10, "Betis-Real Madrid", UtilDate.newDate(year, month, 17));

			Event ev11 = new Event(11, "Atletico-Athletic", UtilDate.newDate(year, month, 1));
			Event ev12 = new Event(12, "Eibar-Barcelona", UtilDate.newDate(year, month, 1));
			Event ev13 = new Event(13, "Getafe-Celta", UtilDate.newDate(year, month, 1));
			Event ev14 = new Event(14, "Alavés-Deportivo", UtilDate.newDate(year, month, 1));
			Event ev15 = new Event(15, "Español-Villareal", UtilDate.newDate(year, month, 1));
			Event ev16 = new Event(16, "Las Palmas-Sevilla", UtilDate.newDate(year, month, 1));

			Event ev17 = new Event(17, "Málaga-Valencia", UtilDate.newDate(year, month + 1, 28));
			Event ev18 = new Event(18, "Girona-Leganés", UtilDate.newDate(year, month + 1, 28));
			Event ev19 = new Event(19, "Real Sociedad-Levante", UtilDate.newDate(year, month + 1, 28));
			Event ev20 = new Event(20, "Betis-Real Madrid", UtilDate.newDate(year, month + 1, 28));

			Question q1;
			Question q2;
			Question q3;
			Question q4;
			Question q5;
			Question q6;

			if (Locale.getDefault().equals(new Locale("es"))) {
				q1 = ev1.addQuestion("¿Quién ganará el partido?", 1);
				q2 = ev1.addQuestion("¿Quién meterá el primer gol?", 2);
				q3 = ev11.addQuestion("¿Quién ganará el partido?", 1);
				q4 = ev11.addQuestion("¿Cuántos goles se marcarán?", 2);
				q5 = ev17.addQuestion("¿Quién ganará el partido?", 1);
				q6 = ev17.addQuestion("¿Habrá goles en la primera parte?", 2);
			} else if (Locale.getDefault().equals(new Locale("en"))) {
				q1 = ev1.addQuestion("Who will win the match?", 1);
				q2 = ev1.addQuestion("Who will score first?", 2);
				q3 = ev11.addQuestion("Who will win the match?", 1);
				q4 = ev11.addQuestion("How many goals will be scored in the match?", 2);
				q5 = ev17.addQuestion("Who will win the match?", 1);
				q6 = ev17.addQuestion("Will there be goals in the first half?", 2);
			} else {
				q1 = ev1.addQuestion("Zeinek irabaziko du partidua?", 1);
				q2 = ev1.addQuestion("Zeinek sartuko du lehenengo gola?", 2);
				q3 = ev11.addQuestion("Zeinek irabaziko du partidua?", 1);
				q4 = ev11.addQuestion("Zenbat gol sartuko dira?", 2);
				q5 = ev17.addQuestion("Zeinek irabaziko du partidua?", 1);
				q6 = ev17.addQuestion("Golak sartuko dira lehenengo zatian?", 2);

			}

			db.persist(q1);
			db.persist(q2);
			db.persist(q3);
			db.persist(q4);
			db.persist(q5);
			db.persist(q6);

			db.persist(ev1);
			db.persist(ev2);
			db.persist(ev3);
			db.persist(ev4);
			db.persist(ev5);
			db.persist(ev6);
			db.persist(ev7);
			db.persist(ev8);
			db.persist(ev9);
			db.persist(ev10);

			db.persist(ev11);
			db.persist(ev12);
			db.persist(ev13);
			db.persist(ev14);
			db.persist(ev15);
			db.persist(ev16);

			db.persist(ev17);
			db.persist(ev18);
			db.persist(ev19);
			db.persist(ev20);

			//db.persist(new Bezeroa(null, null, null, null, null, null, "asierTonto", "topnto", null));
			db.persist(new Bezeroa(null, null, null, null, null, null, "a", "a", null, true));
			db.persist(new Bezeroa(null, null, null, null, null, null, "b", "b", null, true));
			db.persist(new Bezeroa(null, null, null, null, null, null, "c", "c", null, true));
			db.persist(new Bezeroa(null, null, null, null, null, null, "d", "d", null, true));
			db.persist(new Bezeroa(null, null, null, null, null, null, "e", "e", null, true));

			db.persist(new Langilea("Scrum master", null, null, null, null, null, "ekhi", "ekhipo", 10));

			db.getTransaction().commit();
			System.out.println("Db initialized");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method creates a question for an event, with a question text and the
	 * minimum bet
	 * 
	 * @param event      to which question is added
	 * @param question   text of the question
	 * @param betMinimum minimum quantity of the bet
	 * @return the created question, or null, or an exception
	 * @throws QuestionAlreadyExist if the same question already exists for the
	 *                              event
	 */
	public Question createQuestion(Event event, String question, float betMinimum) throws QuestionAlreadyExist {
		System.out.println(">> DataAccess: createQuestion=> event= " + event + " question= " + question + " betMinimum="
				+ betMinimum);

		Event ev = db.find(Event.class, event.getEventNumber());

		if (ev.DoesQuestionExists(question))
			throw new QuestionAlreadyExist(ResourceBundle.getBundle("Etiquetas").getString("ErrorQueryAlreadyExist"));

		db.getTransaction().begin();
		Question q = ev.addQuestion(question, betMinimum);
		// db.persist(q);
		db.persist(ev); // db.persist(q) not required when CascadeType.PERSIST is added in questions
						// property of Event class
						// @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
		db.getTransaction().commit();
		return q;

	}

	public boolean updateDirua(Bezeroa bezero, float diruKop, Date data, String desk) {
		db.getTransaction().begin();
		Bezeroa bez = db.find(bezero.getClass(), bezero.getErabiltzailea());
		bez.updateDirua(diruKop, data, desk);
		db.getTransaction().commit();
		return true;
	}

	public void deleteEvent(Event event) {
		db.getTransaction().begin();
		Event devent = db.find(Event.class, event.getEventNumber());
		db.remove(devent);
		db.getTransaction().commit();
	}

	/**
	 * This method retrieves from the database the events of a given date
	 * 
	 * @param date in which events are retrieved
	 * @return collection of events
	 */
	public Vector<Event> getEvents(Date date) {
		System.out.println(">> DataAccess: getEvents");
		Vector<Event> res = new Vector<Event>();
		TypedQuery<Event> query = db.createQuery("SELECT ev FROM Event ev WHERE ev.eventDate=?1", Event.class);
		query.setParameter(1, date);
		List<Event> events = query.getResultList();
		for (Event ev : events) {
			System.out.println(ev.toString());
			res.add(ev);
		}
		return res;
	}

	/**
	 * This method retrieves from the database the dates a month for which there are
	 * events
	 * 
	 * @param date of the month for which days with events want to be retrieved
	 * @return collection of dates
	 */
	public Vector<Date> getEventsMonth(Date date) {
		System.out.println(">> DataAccess: getEventsMonth");
		Vector<Date> res = new Vector<Date>();

		Date firstDayMonthDate = UtilDate.firstDayMonth(date);
		Date lastDayMonthDate = UtilDate.lastDayMonth(date);

		TypedQuery<Date> query = db.createQuery(
				"SELECT DISTINCT ev.eventDate FROM Event ev WHERE ev.eventDate BETWEEN ?1 and ?2", Date.class);
		query.setParameter(1, firstDayMonthDate);
		query.setParameter(2, lastDayMonthDate);
		List<Date> dates = query.getResultList();
		for (Date d : dates) {
			System.out.println(d.toString());
			res.add(d);
		}
		return res;
	}

	public void open(boolean initializeMode) {

		System.out.println("Opening DataAccess instance => isDatabaseLocal: " + c.isDatabaseLocal()
				+ " getDatabBaseOpenMode: " + c.getDataBaseOpenMode());

		String fileName = c.getDbFilename();
		if (initializeMode) {
			fileName = fileName + ";drop";
			System.out.println("Deleting the DataBase");
		}

		if (c.isDatabaseLocal()) {
			emf = Persistence.createEntityManagerFactory("objectdb:" + fileName);
			db = emf.createEntityManager();
		} else {
			Map<String, String> properties = new HashMap<String, String>();
			properties.put("javax.persistence.jdbc.user", c.getUser());
			properties.put("javax.persistence.jdbc.password", c.getPassword());

			emf = Persistence.createEntityManagerFactory(
					"objectdb://" + c.getDatabaseNode() + ":" + c.getDatabasePort() + "/" + fileName, properties);

			db = emf.createEntityManager();
		}

	}

	public boolean existQuestion(Event event, String question) {
		System.out.println(">> DataAccess: existQuestion=> event= " + event + " question= " + question);
		Event ev = db.find(Event.class, event.getEventNumber());
		return ev.DoesQuestionExists(question);

	}

	public void close() {
		db.close();
		System.out.println("DataBase closed");
	}

	public Pertsona isLogin(String erabiltzailea, String pass) {
		Pertsona p = db.find(Pertsona.class, erabiltzailea);
		if (p != null) {
			if (p.getErabiltzailea().equals(erabiltzailea) && p.getPasahitza().equals(pass)) {
				return p;
			}
		}
		return null;
	}

	public Pertsona libreDago(String erabiltzailea) {
		return db.find(Pertsona.class, erabiltzailea);
	}

	public boolean erregistratu(String dNI, String herrialdea, String izena, String abizena, Date jaiotzeData,
			String korreoa, String erabiltzailea, String pasahitza, String kontuKorrontea, boolean kopiatuBai) {
		try {
			db.getTransaction().begin();
			Bezeroa bezero = new Bezeroa(dNI, herrialdea, izena, abizena, jaiotzeData, korreoa, erabiltzailea,
					pasahitza, kontuKorrontea, kopiatuBai);
			db.persist(bezero);
			db.getTransaction().commit();
		} catch (Exception e) {

			return false;
		}
		return true;
	}

	public boolean createEvent(Integer eventNumber, String description, Date eventDate) {
		try {
			db.getTransaction().begin();
			Event event = new Event(eventNumber, description, eventDate);
			db.persist(event);
			db.getTransaction().commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void deleteApustua(Apustua apustu) {

		db.getTransaction().begin();
		Apustua apustuD = db.find(apustu.getClass(), apustu.getId());
		// Bezeroa bezeroa = db.find(Bezeroa.class,
		// apustuD.getBezero().getErabiltzailea());
		// Kuota kuota = db.find(Kuota.class, apustuD.getKuota().getKuotaZenbakia());
		apustuD.apustuaezabatu();
		// bezeroa.updateDirua(apustuD.getBet(), new Date(), "Apustua ezabatu");
		// bezeroa.getApustuL().remove(apustuD);
		// kuota.getApustuL().remove(apustuD);
		// db.remove(apustuD);
		db.getTransaction().commit();
	}

	public boolean createKuota(Question q, float k, String em) {
		boolean ongi = true;
		try {
			db.getTransaction().begin();
			Question x = db.find(Question.class, q.getQuestionNumber());
			Kuota kuota = x.addNewKuota(k, em);
			db.persist(x);
			db.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			ongi = false;
		}

		return ongi;
	}

	public boolean kuotaLibre(Question q, float k, String em) {
		TypedQuery<Kuota> query = db
				.createQuery("SELECT k FROM Kuota k WHERE k.erantzunP=?1 AND k.kuota=?2 AND k.galdera=?3", Kuota.class);
		query.setParameter(1, em);
		query.setParameter(2, k);
		query.setParameter(3, q);
		List<Kuota> kuotak = query.getResultList();

		if (kuotak.size() == 0)
			return true;

		return false;
	}

	public boolean emaitzaIpini(Question q, String em) {
		boolean ongi = true;
		float biderkatzailea;
		boolean bbbd = false;
		Kuota k1;
		try {
			db.getTransaction().begin();
			Question x = db.find(Question.class, q.getQuestionNumber());
			x.setResult(em);
			for (Kuota k : x.getKuotak()) {

				if (k.getErantzunP().equals(em)) {

					k1 = db.find(Kuota.class, k.getKuotaZenbakia());
					for (Apustua a : k.getApustuL()) {
						bbbd = false;

						biderkatzailea = 1;
						List<Kuota> kuotak = a.getKuota();
						if (!a.getAmaituta()) {

							a.setKont();
							if (kuotak.size() == a.getKont()) {

								if (a.getKopiatua() != null) {
									if (a.getKopiatua().equals(true)) {
										bbbd = true;

										biderkatzailea = biderkatzailea - 0.1f; // apustua kopia bat da
										// a.getBezeroak().get(0).updateDirua(0.1f * a.getBet(), new Date(),
										// "Komisioa");
									}
									/*
									 * if (a.getKopiatua().equals(false)) { System.out.println("7"); biderkatzailea
									 * = biderkatzailea + 0.1f; // apustua beste batek kopiatu du }
									 */
								}
								List<Bezeroa> bezeroak = a.getBezeroak();
								if (bezeroak.size() > 1)
									biderkatzailea = biderkatzailea + 0.1f; // apustu grupala
								for (Kuota i : kuotak)
									biderkatzailea = biderkatzailea * i.getKuota(); // kuota guztiak biderkatu
								/*
								if (bbbd) {
									for (Kopiatu g : a.getBezeroak().get(0).getKopiatzaileak()) {
										g.getKopiatzailea().updateDirua(a.getBet() * biderkatzailea, new Date(),
												"Apustua irabazi");
									}
								}
								*/
								for (Bezeroa b : bezeroak) {

									b.updateDirua(a.getBet() * biderkatzailea, new Date(), "Apustua irabazi");
									if (a.getKopiatua() != null && a.getKopiatua().equals(false)) {
										for (Kopiatu c : b.getKopiatzaileak()) {
											b.updateDirua(c.getZenbat() * a.getBet() * biderkatzailea * 0.1f,new Date(), "Komisioa " + c.getKopiatzailea() + " erabiltzailetik");
										}
									}
								}

								/*
								 * for (Bezeroa b : bezeroak) { b.updateDirua(a.getBet() * k.getKuota() *
								 * biderkatzailea, new Date(), "Apustua irabazi"); }
								 */
								a.setAmaituta(true);
							}
						}
					}
					// break;
				} else {
					for (Apustua a : k.getApustuL()) {
						if (a.getKuota().size() > 1)
							a.setAmaituta(true);
					}
				}
			}
			db.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			ongi = false;
		}

		return ongi;
	}

	public Bezeroa createApustua(Float bet, Date data, Bezeroa b, List<Kuota> k) {
		db.getTransaction().begin();
		Apustua apustua;
		Kuota k1;
		Apustua apustua1 = null;
		Vector<Kuota> kuotak = new Vector<Kuota>();
		for (Kuota i : k) {
			Kuota x = db.find(Kuota.class, i.getKuotaZenbakia());
			kuotak.add(x);
		}
		Bezeroa y = db.find(Bezeroa.class, b.getErabiltzailea());
		try {
			List<Apustua> ap = new Vector<Apustua>();
			List<Bezeroa> bez = new Vector<Bezeroa>();
			bez.add(b);
			if (y.getKopiatzaileak().size() > 0) {
				apustua = new Apustua(bet, data, bez, kuotak, false);
				// apustua1 = new Apustua(bet, data, bez, kuotak, true);
				for (Kuota i : k) {
					Kuota x = db.find(Kuota.class, i.getKuotaZenbakia());
				}

				for (Kopiatu a : y.getKopiatzaileak()) {
					if(a.getKopiatzailea().getDirua()>=(bet*a.getZenbat())) {
						List<Bezeroa> bez1 = new Vector<Bezeroa>();
						bez1.add(a.getKopiatzailea());
						apustua1 = new Apustua(bet * a.getZenbat(), data, bez1, kuotak, true);
						db.persist(apustua1);
						a.getKopiatzailea().addApustu(apustua1);
						a.getKopiatzailea().addNewDiruMugimendua(data, -bet * a.getZenbat(), "Apustuan dirua kendu", a.getKopiatzailea());
						a.getKopiatzailea().setDirua(a.getKopiatzailea().getDirua()-bet * a.getZenbat());
					}
				}
			} else {
				apustua = new Apustua(bet, data, bez, kuotak);
			}
			db.persist(apustua);
			for (Kuota i : kuotak) {
				k1 = db.find(Kuota.class, i.getKuotaZenbakia());
				k1.addNewApustua(apustua);
				if (apustua1 != null)
					k1.addNewApustua(apustua1);
			}
			y.addNewApustua(apustua);
			y.setDirua(y.getDirua() - apustua.getBet());
			y.addNewDiruMugimendua(apustua.getData(), -apustua.getBet(), "Apustuan dirua kendu", b);
			db.persist(y);
			db.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return y;
	}

	public Event eventExist(String description, Date eventDate) {
		Event event = null;
		TypedQuery<Event> query = db.createQuery("SELECT ev FROM Event ev WHERE ev.description=?1 AND ev.eventDate=?2",
				Event.class);
		query.setParameter(1, description);
		query.setParameter(2, eventDate);
		if (!query.getResultList().isEmpty()) {
			event = query.getResultList().get(0);
		}
		return event;
	}

	public Question getGaldera(Kuota kuota) {
		Question q = null;
		db.getTransaction().begin();
		Kuota k = db.find(Kuota.class, kuota.getKuotaZenbakia());
		q = k.getGaldera();
		db.getTransaction().commit();
		return q;
	}

	public Event getEvent(Event event) {
		Event e = null;
		db.getTransaction().begin();
		e = db.find(Event.class, event.getEventNumber());
		db.getTransaction().commit();
		return e;
	}

	public Bezeroa ezabatuMezua(Mezua m, Bezeroa b) {
		db.getTransaction().begin();
		Bezeroa b1= db.find(b.getClass(), b.getErabiltzailea());
		Mezua mezua = db.find(m.getClass(), m.getMezuZenbakia());
		b1.getMezuak().remove(mezua);
		db.remove(mezua);
		db.getTransaction().commit();
		return b;
	}

	public Bezeroa onartuMezua(MezuLaguna m, Bezeroa b) {
		db.getTransaction().begin();
		MezuLaguna m1 = db.find(m.getClass(), m.getMezuZenbakia());
		Bezeroa b1 = db.find(b.getClass(), b.getErabiltzailea());
		Bezeroa nork = m1.getNork();
		b1.addLaguna(nork);
		nork.addLaguna(b1);
		b1.ezabatuMezua(m1);
		db.remove(m1);
		db.getTransaction().commit();
		return nork;
	}

	public Bezeroa onartuMezua(MezuGrupala m, Bezeroa b) {
		db.getTransaction().begin();
		MezuGrupala m1 = db.find(MezuGrupala.class, m.getMezuZenbakia());
		Bezeroa b1 = db.find(Bezeroa.class, b.getErabiltzailea());
		Apustua a = db.find(Apustua.class, m1.getApustua().getId());
		if(b1.getDirua()>=a.getBet()) {
			a.addBezeroa(b1);
			b1.addApustu(a);
			b1.addNewDiruMugimendua(new Date(), -a.getBet(), "Apustu grupala egin", b1);
			b1.setDirua(b1.getDirua()-a.getBet());
			db.remove(m1);
		}else{
			db.getTransaction().commit();
			return null;
		}
		db.getTransaction().commit();
		return b1;
	}

	public Mezua lagunEskaeraEgin(Bezeroa b, String erab) {
		db.getTransaction().begin();
		Bezeroa b1 = db.find(Bezeroa.class, b.getErabiltzailea());
		Bezeroa e = db.find(Bezeroa.class, erab);
		Mezua m = new MezuLaguna(b1, e, new Date());
		db.persist(m);
		e.addMezua(m);
		db.getTransaction().commit();
		return m;
	}

	public Pertsona bilatuErabiltzailea(String erab) {
		return db.find(Pertsona.class, erab);
	}

	public List<Bezeroa> bilatuErabiltzaileak(String erab) {

		List<Bezeroa> emaitza = new Vector<Bezeroa>();
		TypedQuery<Bezeroa> query = db.createQuery(
				"SELECT b FROM Bezeroa b WHERE b.Erabiltzailea LIKE CONCAT('%',:izena,'%')", Bezeroa.class);
		query.setParameter("izena", erab);
		if (!query.getResultList().isEmpty()) {
			emaitza = query.getResultList();
			for (Bezeroa a : emaitza) {
				System.out.println(a.getErabiltzailea());
			}
		} else {
		}
		return emaitza;
	}

	public Kopiatu kopiatuEgin(Bezeroa u, Bezeroa e, float z) {
		/*
		 * List<Apustua> apustuak = u.addApustuenKopiak(e, z); if (apustuak != null &&
		 * apustuak.size() != 0) { for (Apustua a : apustuak) {
		 * createApustua(a.getBet(), new Date(), u, a.getKuota()); } }
		 */
		db.getTransaction().begin();
		Bezeroa kopiatua = db.find(Bezeroa.class, e.getErabiltzailea());
		Bezeroa kopiatzailea = db.find(Bezeroa.class, u.getErabiltzailea());
		Kopiatu k = new Kopiatu(kopiatzailea, kopiatua, z);
		db.persist(k);
		kopiatua.addKopiatzailea(k);
		kopiatzailea.addaaab(k);
		db.getTransaction().commit();
		return k;
		// }
		// return null;
	}

	public boolean mezuaBidali(Bezeroa b, Mezua m) {
		db.getTransaction().begin();
		db.persist(m);
		Mezua m1=db.find(m.getClass(), m.getMezuZenbakia());
		Bezeroa bez = db.find(Bezeroa.class, b);
		bez.addMezua(m1);
		db.getTransaction().commit();
		return false;
	}
/*
	public void mezuaBidali(Bezeroa l, Bezeroa be) {
		db.getTransaction().begin();
		Bezeroa user= db.find(l.getClass(), be.getErabiltzailea());
		Apustua azkena= user.getLastApustu();
		Bezeroa lagun= db.find(l.getClass(), l.getErabiltzailea());
		MezuGrupala m=new MezuGrupala(user,lagun,new Date());
		db.persist(m);
		System.out.println(m.getmezuZenbakiaGrupalaaaaaa());
		System.out.println(MezuGrupala.class);
		MezuGrupala m1=db.find(MezuGrupala.class, m.getmezuZenbakiaGrupalaaaaaa());
		m1.setApustua(azkena);
		lagun.addMezua(m1);
		db.getTransaction().commit();
	}
*/
	public boolean mezuaBidali(Bezeroa b, MezuGrupala m, Apustua a) {
		db.getTransaction().begin();
		db.persist(m);
		m.setApustua(a);
		Bezeroa bez = db.find(Bezeroa.class, b);
		bez.addMezua(m);
		db.getTransaction().commit();
		return false;
	}

	public Mezua getMezu(int mz) {
		db.getTransaction().begin();
		Mezua m= db.find(Mezua.class,mz );
		db.getTransaction().commit();
		return m;
	}
}
