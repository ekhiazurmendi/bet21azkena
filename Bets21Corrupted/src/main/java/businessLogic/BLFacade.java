package businessLogic;

import java.util.Vector;
import exceptions.*;
import java.util.Date;
import java.util.List;

import domain.*;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Interface that specifies the business logic.
 */
@WebService
public interface BLFacade  {
	  

	/**
	 * This method creates a question for an event, with a question text and the minimum bet
	 * 
	 * @param event to which question is added
	 * @param question text of the question
	 * @param betMinimum minimum quantity of the bet
	 * @return the created question, or null, or an exception
	 * @throws EventFinished if current data is after data of the event
 	 * @throws QuestionAlreadyExist if the same question already exists for the event
	 */
	@WebMethod Question createQuestion(Event event, String question, float betMinimum) throws EventFinished, QuestionAlreadyExist;
	
	
	/**
	 * This method retrieves the events of a given date 
	 * 
	 * @param date in which events are retrieved
	 * @return collection of events
	 */
	@WebMethod public Vector<Event> getEvents(Date date);
	
	@WebMethod public Pertsona isLogin(String erabiltzailea, String pasahitza);
	
	@WebMethod public boolean erregistratu(String dni, String herrialdea, String izena,
			String abizena, Date jaiotzeData, String korreoa, String erabiltzailea, String pasahitza, String kontuKorrontea, boolean kopiatuBai);
	
	/**
	 * This method retrieves from the database the dates a month for which there are events
	 * 
	 * @param date of the month for which days with events want to be retrieved 
	 * @return collection of dates
	 */
	@WebMethod public Vector<Date> getEventsMonth(Date date);
	
	/**
	 * This method calls the data access to initialize the database with some events and questions.
	 * It is invoked only when the option "initialize" is declared in the tag dataBaseOpenMode of resources/config.xml file
	 */	
	@WebMethod public void initializeBD();
	
	@WebMethod public boolean createEvent(Integer eventNumber, String description,Date eventDate) throws EventAlreadyExist;

	@WebMethod public boolean updateDirua(Bezeroa bezero, float diruKop, Date date, String desk);
	
	@WebMethod public void deleteEvent(Event event);
	
	@WebMethod public void deleteApustu(Apustua apustu);
	
	@WebMethod public int createKuota(Question q, float k, String em);
	
	@WebMethod public boolean emaitzaIpini(Question q, String em);
	
	@WebMethod public Bezeroa createApustua(Float bet, Date data, Bezeroa b, List<Kuota> k, boolean grupala);

	@WebMethod public Bezeroa updateBezero(Bezeroa u);
	
	@WebMethod public Question getGaldera(Kuota kuota);

	@WebMethod public Bezeroa mezuaErantzun(int a, boolean o, Bezeroa u);
	
	@WebMethod public boolean lagunaEgin(Bezeroa u, String erab);
	
	@WebMethod public boolean kopiatuEgin(Bezeroa u, String erab, float z);
	
	@WebMethod public Pertsona updateUser(String string);
	
	@WebMethod public List<Mezua> getMezuak(Bezeroa u);
	
	@WebMethod public List<Bezeroa> getBezeroak(String erab);


	@WebMethod public List<Bezeroa> getLagunak(String erab);


	@WebMethod public boolean kopiaDaiteke(String erabiltzailea);
}
