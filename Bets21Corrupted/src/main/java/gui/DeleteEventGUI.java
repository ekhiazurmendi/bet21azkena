package gui;

import businessLogic.BLFacade;
import businessLogic.BLFacadeImplementation;
import configuration.UtilDate;

import com.toedter.calendar.JCalendar;

import domain.Bezeroa;
import domain.Langilea;
import domain.Question;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.text.DateFormat;
import java.util.*;

import javax.swing.table.DefaultTableModel;

public class DeleteEventGUI extends JFrame {
	private BLFacade BLogic= MainGUI.getBusinessLogic();
	public static DeleteEventGUI deteleteEventgui;
	private static final long serialVersionUID = 1L;
	private boolean Enabled = false;
	private final JLabel jLabelEventDate = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EventDate"));
	private final JLabel jLabelEvents = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Events"));
	
	private Date firstDay;
	
	private JButton jButtonClose = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Close"));

	// Code for JCalendar
	private JCalendar jCalendar1 = new JCalendar();
	private Calendar calendarAnt = null;
	private Calendar calendarAct = null;
	private JScrollPane scrollPaneEvents = new JScrollPane();

	private Vector<Date> datesWithEventsCurrentMonth = new Vector<Date>();

	private JTable tableEvents = new JTable();

	private DefaultTableModel tableModelEvents;
	private DefaultTableModel tableModelQueries;

	private String[] columnNamesEvents = new String[] { ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EventN"),
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Event"),

	};
	private String[] columnNamesQueries = new String[] { ResourceBundle.getBundle(MainGUI.hizkuntza).getString("QueryN"),
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Query")

	};
	private final JButton DeleteEventButton = new JButton(
			//ResourceBundle.getBundle("Etiquetas").getString("DeleteEventGUI.btnNewButton.text")
			"Delete Event"
			); //$NON-NLS-1$ //$NON-NLS-2$
	private final JLabel ErrorLabel = new JLabel(
			//(ResourceBundle.getBundle("Etiquetas").getString("DeleteEventGUI.lblNewLabel.text")
			); //$NON-NLS-1$ //$NON-NLS-2$

	public DeleteEventGUI() {
		deteleteEventgui=this;
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jbInit() throws Exception {

		this.getContentPane().setLayout(null);
		this.setSize(new Dimension(700, 500));
		this.setTitle(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("QueryQueries"));

		jLabelEventDate.setBounds(new Rectangle(40, 15, 140, 25));
		jLabelEvents.setBounds(295, 19, 259, 16);

		this.getContentPane().add(jLabelEventDate, null);
		this.getContentPane().add(jLabelEvents);

		jButtonClose.setBounds(new Rectangle(274, 419, 130, 30));

		jButtonClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton2_actionPerformed(e);
			}
		});

		this.getContentPane().add(jButtonClose, null);

		jCalendar1.setBounds(new Rectangle(40, 50, 225, 150));

		BLFacade facade = MainGUI.getBusinessLogic();
		datesWithEventsCurrentMonth = facade.getEventsMonth(jCalendar1.getDate());
		CreateQuestionGUI.paintDaysWithEvents(jCalendar1, datesWithEventsCurrentMonth);

		// Code for JCalendar
		this.jCalendar1.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent propertychangeevent) {

				if (propertychangeevent.getPropertyName().equals("locale")) {
					jCalendar1.setLocale((Locale) propertychangeevent.getNewValue());
				} else if (propertychangeevent.getPropertyName().equals("calendar")) {
					calendarAnt = (Calendar) propertychangeevent.getOldValue();
					calendarAct = (Calendar) propertychangeevent.getNewValue();
					DateFormat dateformat1 = DateFormat.getDateInstance(1, jCalendar1.getLocale());
//					jCalendar1.setCalendar(calendarAct);
					firstDay = UtilDate.trim(new Date(jCalendar1.getCalendar().getTime().getTime()));

					int monthAnt = calendarAnt.get(Calendar.MONTH);
					int monthAct = calendarAct.get(Calendar.MONTH);

					if (monthAct != monthAnt) {
						if (monthAct == monthAnt + 2) {
							// Si en JCalendar está 30 de enero y se avanza al mes siguiente, devolvería 2
							// de marzo (se toma como equivalente a 30 de febrero)
							// Con este código se dejará como 1 de febrero en el JCalendar
							calendarAct.set(Calendar.MONTH, monthAnt + 1);
							calendarAct.set(Calendar.DAY_OF_MONTH, 1);
						}

						jCalendar1.setCalendar(calendarAct);

						BLFacade facade = MainGUI.getBusinessLogic();

						datesWithEventsCurrentMonth = facade.getEventsMonth(jCalendar1.getDate());
					}

					CreateQuestionGUI.paintDaysWithEvents(jCalendar1, datesWithEventsCurrentMonth);

					try {
						tableModelEvents.setDataVector(null, columnNamesEvents);
						tableModelEvents.setColumnCount(3); // another column added to allocate ev objects

						BLFacade facade = MainGUI.getBusinessLogic();

						Vector<domain.Event> events = facade.getEvents(firstDay);

						if (events.isEmpty())
							jLabelEvents.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NoEvents") + ": "
									+ dateformat1.format(calendarAct.getTime()));
						else
							jLabelEvents.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Events") + ": "
									+ dateformat1.format(calendarAct.getTime()));
						for (domain.Event ev : events) {
							Vector<Object> row = new Vector<Object>();

							System.out.println("Events " + ev);

							row.add(ev.getEventNumber());
							row.add(ev.getDescription());
							row.add(ev); // ev object added in order to obtain it with tableModelEvents.getValueAt(i,2)
							tableModelEvents.addRow(row);
						}
						tableEvents.getColumnModel().getColumn(0).setPreferredWidth(25);
						tableEvents.getColumnModel().getColumn(1).setPreferredWidth(268);
						tableEvents.getColumnModel().removeColumn(tableEvents.getColumnModel().getColumn(2)); // not
																												// shown
																												// in
																												// JTable
					} catch (Exception e1) {

						// jLabelQueries.setText(e1.getMessage());
					}

				}
			}
		});

		this.getContentPane().add(jCalendar1, null);

		scrollPaneEvents.setBounds(new Rectangle(292, 50, 346, 150));

		ErrorLabel.setBounds(228, 236, 225, 14);
		ErrorLabel.setVisible(false);
		getContentPane().add(ErrorLabel);

		tableEvents.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = tableEvents.getSelectedRow();
				domain.Event ev = (domain.Event) tableModelEvents.getValueAt(i, 2); // obtain ev object
				/*
				 * Vector<Question> queries=ev.getQuestions();
				 * 
				 * tableModelQueries.setDataVector(null, columnNamesQueries);
				 * 
				 * if (queries.isEmpty())
				 * jLabelQueries.setText(ResourceBundle.getBundle("Etiquetas").getString(
				 * "NoQueries")+": "+ev.getDescription()); else
				 * jLabelQueries.setText(ResourceBundle.getBundle("Etiquetas").getString(
				 * "SelectedEvent")+" "+ev.getDescription());
				 * 
				 * for (domain.Question q:queries){ Vector<Object> row = new Vector<Object>();
				 * 
				 * row.add(q.getQuestionNumber()); row.add(q.getQuestion());
				 * tableModelQueries.addRow(row); }
				 * tableQueries.getColumnModel().getColumn(0).setPreferredWidth(25);
				 * tableQueries.getColumnModel().getColumn(1).setPreferredWidth(268);
				 */
			}

		});

		scrollPaneEvents.setViewportView(tableEvents);
		tableModelEvents = new DefaultTableModel(null, columnNamesEvents);

		tableEvents.setModel(tableModelEvents);
		tableEvents.getColumnModel().getColumn(0).setPreferredWidth(25);
		tableEvents.getColumnModel().getColumn(1).setPreferredWidth(268);
		tableModelQueries = new DefaultTableModel(null, columnNamesQueries);

		//////////////////////////////////////////////////////////////////////////

		this.getContentPane().add(scrollPaneEvents, null);
		DeleteEventButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ErrorLabel.setVisible(false);
				if (!(tableEvents.getSelectedRow() == -(1))) {
					int i = tableEvents.getSelectedRow();
					domain.Event ev = (domain.Event) tableModelEvents.getValueAt(i, 2); // Hautatutako event-a hartzen du
					BLogic.deleteEvent(ev);		
					tableModelEvents.removeRow(i);
				} else {
					ErrorLabel.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("OndoAukeratuGertaera"));
					ErrorLabel.setForeground(Color.RED);
					ErrorLabel.setVisible(true);
				}
			}
		});
		DeleteEventButton.setBounds(228, 259, 225, 88);

		getContentPane().add(DeleteEventButton);

		////////////////////////////////////////////////////////////////////////
	}

	private void jButton2_actionPerformed(ActionEvent e) {
		LangileGUI.langileaGUI.setVisible(true);
		LoginGUI.logeatua =(Langilea)MainGUI.getBusinessLogic().updateUser(LoginGUI.logeatua.Erabiltzailea);
		dispose();
    
	}

}
