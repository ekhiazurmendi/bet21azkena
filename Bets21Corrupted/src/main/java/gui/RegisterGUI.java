package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.BLFacade;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JCheckBox;

public class RegisterGUI extends JFrame {

	public static RegisterGUI registerGui;
	
	private JPanel contentPane;
	private JTextField ERABILTZAILEA;
	private JTextField IZENA;
	private JTextField ABIZENA;
	private JTextField NAN;
	private JTextField KORREOA;
	private JPasswordField PASAHITZA;
	private JPasswordField PASAHITZA_2;
	private JTextField HERRIALDEA;
	private JTextField JAIOTZEDATA;
	private JTextField textField_1;
	private JCheckBox chckbxKopiatu = new JCheckBox(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("KopiaBai"));

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterGUI frame = new RegisterGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterGUI() {
		
		registerGui = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 529, 750);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox HizkuntzaBox = new JComboBox();
		HizkuntzaBox.setBounds(395, 0, 118, 20);
		contentPane.add(HizkuntzaBox);
		
		JLabel lblErabiltzailea = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERABILTZAILEA"));
		lblErabiltzailea.setBounds(10, 76, 93, 14);
		contentPane.add(lblErabiltzailea);
		
		ERABILTZAILEA = new JTextField();
		ERABILTZAILEA.setBounds(161, 73, 173, 20);
		contentPane.add(ERABILTZAILEA);
		ERABILTZAILEA.setColumns(10);
		
		JLabel lblIzena = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("IZENA"));
		lblIzena.setBounds(10, 104, 63, 14);
		contentPane.add(lblIzena);
		
		IZENA = new JTextField();
		IZENA.setColumns(10);
		IZENA.setBounds(161, 101, 173, 20);
		contentPane.add(IZENA);
		
		JLabel lblAbizena = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ABIZENA"));
		lblAbizena.setBounds(10, 135, 73, 14);
		contentPane.add(lblAbizena);
		
		ABIZENA = new JTextField();
		ABIZENA.setColumns(10);
		ABIZENA.setBounds(161, 132, 173, 20);
		contentPane.add(ABIZENA);
		
		JLabel lblNan = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NAN"));
		lblNan.setBounds(10, 163, 73, 14);
		contentPane.add(lblNan);
		
		NAN = new JTextField();
		NAN.setColumns(10);
		NAN.setBounds(161, 160, 173, 20);
		contentPane.add(NAN);
		
		JLabel lblKorreoElektronikoa = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("KORREOE"));
		lblKorreoElektronikoa.setBounds(10, 191, 153, 14);
		contentPane.add(lblKorreoElektronikoa);
		
		KORREOA = new JTextField();
		KORREOA.setColumns(10);
		KORREOA.setBounds(161, 188, 173, 20);
		contentPane.add(KORREOA);
		
		JLabel lblPasahitza = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZA"));
		lblPasahitza.setBounds(10, 222, 73, 14);
		contentPane.add(lblPasahitza);
		
		PASAHITZA = new JPasswordField();
		PASAHITZA.setBounds(161, 219, 173, 20);
		contentPane.add(PASAHITZA);
		
		JLabel lblPasahitzaEgiaztatu = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZAEGIAZTATU"));
		lblPasahitzaEgiaztatu.setBounds(10, 250, 134, 14);
		contentPane.add(lblPasahitzaEgiaztatu);
		
		PASAHITZA_2 = new JPasswordField();
		PASAHITZA_2.setBounds(161, 247, 173, 20);
		contentPane.add(PASAHITZA_2);
		
		JLabel lblHerrialdea = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("HERRIALDEA"));
		lblHerrialdea.setBounds(10, 278, 73, 14);
		contentPane.add(lblHerrialdea);
		
		HERRIALDEA = new JTextField();
		HERRIALDEA.setColumns(10);
		HERRIALDEA.setBounds(161, 275, 173, 20);
		contentPane.add(HERRIALDEA);
		
		JAIOTZEDATA = new JTextField();
		JAIOTZEDATA.setColumns(10);
		JAIOTZEDATA.setBounds(161, 303, 173, 20);
		contentPane.add(JAIOTZEDATA);
		
		JLabel lblJaiotzeData = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("JAIOD"));
		lblJaiotzeData.setBounds(12, 306, 112, 14);
		contentPane.add(lblJaiotzeData);
		
		JLabel lblKontuKorrontea = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("KONTUKORRONTEA"));
		lblKontuKorrontea.setBounds(10, 331, 140, 14);
		contentPane.add(lblKontuKorrontea);
		
		JLabel lblDerrigorrezkoa = new JLabel("");
		lblDerrigorrezkoa.setForeground(new Color(204, 0, 0));
		lblDerrigorrezkoa.setBounds(344, 76, 159, 14);
		contentPane.add(lblDerrigorrezkoa);
		
		JLabel lblDerrigorrezkoa_1 = new JLabel("");
		lblDerrigorrezkoa_1.setForeground(new Color(204, 0, 0));
		lblDerrigorrezkoa_1.setBounds(344, 222, 93, 14);
		contentPane.add(lblDerrigorrezkoa_1);
		
		JLabel lblDerrigorrezkoa_2 = new JLabel("");
		lblDerrigorrezkoa_2.setForeground(new Color(204, 0, 0));
		lblDerrigorrezkoa_2.setBounds(344, 250, 110, 14);
		contentPane.add(lblDerrigorrezkoa_2);
		
		JLabel lblDerrigorrezkoa_3 = new JLabel("");
		lblDerrigorrezkoa_3.setForeground(new Color(204, 0, 0));
		lblDerrigorrezkoa_3.setBounds(344, 424, 110, 14);
		contentPane.add(lblDerrigorrezkoa_3);
		
		JLabel lblDataException = new JLabel("");
		lblDataException.setForeground(new Color(204, 0, 0));
		lblDataException.setBounds(375, 306, 110, 14);
		contentPane.add(lblDataException);
		
		JCheckBox chckbxPolitikak = new JCheckBox(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("POLITIKA"));
		chckbxPolitikak.setBounds(38, 457, 296, 23);
		contentPane.add(chckbxPolitikak);

		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(161, 331, 173, 20);
		contentPane.add(textField_1);
		
		JButton RegisterButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERREGISTRATU"));
		RegisterButton.setBounds(145, 375, 153, 33);
		RegisterButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					boolean ondo = false;
					BLFacade facade = MainGUI.getBusinessLogic();
					String a = registerGui.ERABILTZAILEA.getText();
					String b = registerGui.PASAHITZA.getText();
					String c = registerGui.PASAHITZA_2.getText();
					String d = registerGui.IZENA.getText();
					String e = registerGui.HERRIALDEA.getText();
					String f = registerGui.ABIZENA.getText();
					String g = registerGui.JAIOTZEDATA.getText();
					String h = registerGui.KORREOA.getText();
					String i = registerGui.textField_1.getText();
					String j = registerGui.NAN.getText();
					boolean k= registerGui.chckbxKopiatu.isSelected();
					lblDerrigorrezkoa.setText("");
					lblDerrigorrezkoa_1.setText("");
					lblDerrigorrezkoa_2.setText("");
					lblDerrigorrezkoa_3.setText("");
					lblDataException.setText("");
					
					if(!a.isEmpty() && !b.isEmpty() && !c.isEmpty() && b.equals(c) && chckbxPolitikak.isSelected()) {
						try {
							if(g.isEmpty()) {
								ondo = facade.erregistratu(j, e, d, f, null, h, a, b, i, k);
							}else {
								ondo = facade.erregistratu(j, e, d, f, new Date(g), h, a, b, i, k);
							}
							if(ondo) {
								System.out.println("Erregistratu da");
								LoginGUI.loginGui.setVisible(true);
								dispose();
							}else {
								lblDerrigorrezkoa.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERABILTZAILEAH"));
							}
						}catch (IllegalArgumentException m) {
							lblDataException.setText("yyyy/mm/dd");
						}
					}else {
						lblDerrigorrezkoa.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DERRIGORREZKO"));
						lblDerrigorrezkoa_1.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DERRIGORREZKO"));
						lblDerrigorrezkoa_2.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DERRIGORREZKO"));
						lblDerrigorrezkoa_3.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DERRIGORREZKO"));
					}
				}
			});
		contentPane.add(RegisterButton);
		
		JCheckBox chckbxMezuakJasotzeaOnartzen = new JCheckBox(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("MEZUAKHONARTU"));
		chckbxMezuakJasotzeaOnartzen.setSelected(true);
		chckbxMezuakJasotzeaOnartzen.setBounds(38, 483, 258, 23);
		contentPane.add(chckbxMezuakJasotzeaOnartzen);
		
		JLabel lblPasahitzaAhaztu = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("JADAKONTUA"));
		lblPasahitzaAhaztu.setForeground(Color.BLUE.brighter());
		lblPasahitzaAhaztu.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblPasahitzaAhaztu.setBounds(314, 359, 110, 14);
		lblPasahitzaAhaztu.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				LoginGUI.loginGui.setVisible(true);
				//registerGui.setVisible(false);
				dispose();
		    }
		});
		contentPane.add(lblPasahitzaAhaztu);
		
		
		chckbxKopiatu.setBounds(38, 424, 97, 23);
		contentPane.add(chckbxKopiatu);
		
		
		
		
		
		
	}
}
