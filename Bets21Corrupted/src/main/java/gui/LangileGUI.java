package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import domain.Event;
import domain.Pertsona;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class LangileGUI extends JFrame {

	public static LangileGUI langileaGUI;
	
	private Pertsona per;
	
	private JPanel contentPane;

	/*
	 * Launch the application.
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LangileGUI frame = new LangileGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public LangileGUI(Pertsona p) {
		langileaGUI = this;
		per=p;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("QueryQuestions"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				langileaGUI.setVisible(false);
				JFrame a = new FindQuestionsGUI(1);
				
				a.setVisible(true);
			}
		});
		btnNewButton.setBounds(10, 54, 414, 53);
		contentPane.add(btnNewButton);

		JButton btnCreateevents = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("CreateEvents"));
		btnCreateevents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				langileaGUI.setVisible(false);
				Vector<domain.Event> v = new Vector<domain.Event>();
				JFrame a = new CreateEventGUI(v);
				a.setVisible(true);
			}
		});
		btnCreateevents.setBounds(10, 118, 414, 53);
		contentPane.add(btnCreateevents);

		JButton btnNewButton_1 = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("CreateQuery"));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				langileaGUI.setVisible(false);
				JFrame a = new CreateQuestionGUI(new Vector<Event>());

				a.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(10, 181, 414, 53);
		contentPane.add(btnNewButton_1);

		JLabel lblSelectOption = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("SelectOption"));
		lblSelectOption.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSelectOption.setBounds(152, 0, 172, 53);
		contentPane.add(lblSelectOption);
		
		JButton btnItzuli = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("saioaItxi"));
		btnItzuli.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginGUI.logeatua=null;
				//langileaGUI.setVisible(false);
				LoginGUI.loginGui.setVisible(true);
				dispose();
			}
		});
		btnItzuli.setBounds(10, 15, 119, 23);
		contentPane.add(btnItzuli);
		
		JButton DeleteEventButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DeleteEvent"));
		DeleteEventButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				langileaGUI.setVisible(false);
				JFrame a = new DeleteEventGUI();
				a.setVisible(true);
			}
		});
		DeleteEventButton.setBounds(10, 245, 414, 53);
		contentPane.add(DeleteEventButton);
	}
}
