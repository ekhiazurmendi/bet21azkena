package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import businessLogic.BLFacade;
import domain.Bezeroa;
import domain.DiruMugimendua;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class MugimenduGUI extends JFrame {
	public static MugimenduGUI Mugui;
	private BLFacade BLogic= MainGUI.getBusinessLogic();
	private JPanel contentPane;
	private Collection<DiruMugimendua> list;
	private JTable tableMugimenduak = new JTable();
	
	private String[] columnNamesApustuak = new String[] { ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Data"),
			ResourceBundle.getBundle("Etiquetas").getString("Dirua"), ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Deskripzioa")
	};

	private DefaultTableModel tableModelMugimenduak;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MugimenduGUI frame = new MugimenduGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MugimenduGUI() {
		Mugui=this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 553, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	
		JButton CloseButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli"));
		CloseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginGUI.logeatua =(Bezeroa)MainGUI.getBusinessLogic().updateUser(LoginGUI.logeatua.Erabiltzailea);
				UserGUI.usergui.setVisible(true);
				dispose();
		    }
		});
		CloseButton.setBounds(196, 233, 144, 17);
		contentPane.add(CloseButton);
		
		JTextPane EtekinText = new JTextPane();
		EtekinText.setBounds(350, 164, 165, 20);
		contentPane.add(EtekinText);
		
		
		
		JButton EtekinButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EtekinakKalkulatu"));
		EtekinButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EtekinText.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ZureEtekina")+": "+((Bezeroa)LoginGUI.logeatua).getEtekinak());
			}
		});
		EtekinButton.setBounds(350, 130, 165, 23);
		contentPane.add(EtekinButton);
		
	
		
		JLabel lblNewLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("DiruMugimenduak"));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel.setBounds(20, 40, 144, 14);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 65, 320, 157);
		contentPane.add(scrollPane);
		
		scrollPane.setViewportView(tableMugimenduak);
		tableModelMugimenduak = new DefaultTableModel(null, columnNamesApustuak);
		tableMugimenduak.setModel(tableModelMugimenduak);
		list = ((Bezeroa)LoginGUI.logeatua).getDiruML();
		
		tableModelMugimenduak.setColumnCount(3);
		tableModelMugimenduak.setDataVector(null, columnNamesApustuak);

		for (domain.DiruMugimendua diru : list) {
			Vector<Object> row = new Vector<Object>();

			//System.out.println("Kuotak " + k);

			row.add(diru.getData());
			row.add(diru.getZenbat());
			row.add(diru.getDesc());
			//row.add(k); // ev object added in order to obtain it with tableModelEvents.getValueAt(i,2)
			tableModelMugimenduak.addRow(row);
		}
		System.out.println(tableMugimenduak.getColumnModel().getColumnCount());
		tableMugimenduak.getColumnModel().getColumn(0).setPreferredWidth(250);
		tableMugimenduak.getColumnModel().getColumn(1).setPreferredWidth(125);
		tableMugimenduak.getColumnModel().getColumn(2).setPreferredWidth(300);
	
	}
}
