package gui;

import businessLogic.BLFacade;
import configuration.UtilDate;

import com.toedter.calendar.JCalendar;

import domain.Bezeroa;
import domain.Kuota;
import domain.Langilea;
import domain.Question;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.text.DateFormat;
import java.util.*;
import java.util.List;

import javax.swing.table.DefaultTableModel;

public class FindQuestionsGUIBezero extends JFrame {
	private static final long serialVersionUID = 1L;

	private static FindQuestionsGUIBezero findquestionsguiBezero;

	private List<Kuota> apustuAnizkoitz = new Vector<Kuota>();
	private DefaultComboBoxModel<String> KuotaModel = new DefaultComboBoxModel<String>();

	private int mota;
	private Question selectedQuestion;
	private Kuota selectedKuota;
	private Vector<Question> queries;
	private List<Kuota> Kuotak;

	private final JLabel jLabelEventDate = new JLabel(
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EventDate"));
	private final JLabel jLabelQueries = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Queries"));
	private final JLabel jLabelEvents = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Events"));

	private JButton jButtonClose = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Close"));
	private JButton btnKuotakErakutsiButton = new JButton(
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ApostuaEgin"));

	// Code for JCalendar
	private JCalendar jCalendar1 = new JCalendar();
	private Calendar calendarAnt = null;
	private Calendar calendarAct = null;
	private JScrollPane scrollPaneEvents = new JScrollPane();
	private JScrollPane scrollPaneQueries = new JScrollPane();

	private Vector<Date> datesWithEventsCurrentMonth = new Vector<Date>();

	private JTable tableEvents = new JTable();
	private JTable tableQueries = new JTable();

	private DefaultTableModel tableModelEvents;
	private DefaultTableModel tableModelQueries;

	private String[] columnNamesEvents = new String[] { ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EventN"),
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Event"),

	};
	private String[] columnNamesQueries = new String[] {
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("QueryN"),
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Query"),

	};

	private String[] columnNamesKuotak = new String[] {
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("KuotaZenb"),
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Kuota"),
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Emaitza"), };

	private DefaultTableModel tableModelKuotak;
	private JTable tableKuotak = new JTable();
	private final JScrollPane scrollPane = new JScrollPane();
	private final JLabel lblKuotakLabel = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Kuotak")); //$NON-NLS-1$ //$NON-NLS-2$
	private final JButton DeleteAnizkoitzButton = new JButton(
			ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EzabatuAnizkoitza"));

	public FindQuestionsGUIBezero(int m) {
		try {
			jbInit();
			mota = m;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jbInit() throws Exception {

		this.getContentPane().setLayout(null);
		this.setSize(new Dimension(902, 500));
		this.setTitle(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("QueryQueries"));

		JComboBox AnizkoitzBox = new JComboBox();
		AnizkoitzBox.setBounds(707, 83, 152, 20);
		getContentPane().add(AnizkoitzBox);

		JButton AddAnizkoitzButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("AddAnizkoitz"));
		AddAnizkoitzButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//if (!apustuAnizkoitz.contains(selectedKuota)) {
					apustuAnizkoitz.add(selectedKuota);
					KuotaModel.addElement(selectedKuota.getErantzunP());
				//}
				AnizkoitzBox.setModel(KuotaModel);
			}
		});
		AddAnizkoitzButton.setBounds(707, 49, 140, 23);
		getContentPane().add(AddAnizkoitzButton);

		jLabelEventDate.setBounds(new Rectangle(40, 15, 140, 25));
		jLabelQueries.setBounds(40, 248, 406, 14);
		jLabelEvents.setBounds(295, 19, 259, 16);
		btnKuotakErakutsiButton.setEnabled(false);
		btnKuotakErakutsiButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (apustuAnizkoitz.size() == 0) {
					apustuAnizkoitz.add(selectedQuestion.getKuotak().get(tableKuotak.getSelectedRow()));
					JFrame b = new ApustuaGUI(apustuAnizkoitz);
					b.setVisible(true);
				} else {
					JFrame b = new ApustuaGUI(apustuAnizkoitz);
					b.setVisible(true);
				}
			}
		});
		btnKuotakErakutsiButton.setBounds(707, 244, 125, 23);

		this.getContentPane().add(jLabelEventDate, null);
		this.getContentPane().add(jLabelQueries);
		this.getContentPane().add(jLabelEvents); // $NON-NLS-1$ //$NON-NLS-2$
		this.getContentPane().add(btnKuotakErakutsiButton);
		// }

		jButtonClose.setBounds(new Rectangle(366, 420, 130, 30));

		jButtonClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton2_actionPerformed(e);
			}
		});

		this.getContentPane().add(jButtonClose, null);

		jCalendar1.setBounds(new Rectangle(40, 50, 225, 150));

		BLFacade facade = MainGUI.getBusinessLogic();
		datesWithEventsCurrentMonth = facade.getEventsMonth(jCalendar1.getDate());
		CreateQuestionGUI.paintDaysWithEvents(jCalendar1, datesWithEventsCurrentMonth);

		// Code for JCalendar
		this.jCalendar1.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent propertychangeevent) {

				if (propertychangeevent.getPropertyName().equals("locale")) {
					jCalendar1.setLocale((Locale) propertychangeevent.getNewValue());
				} else if (propertychangeevent.getPropertyName().equals("calendar")) {
					calendarAnt = (Calendar) propertychangeevent.getOldValue();
					calendarAct = (Calendar) propertychangeevent.getNewValue();
					DateFormat dateformat1 = DateFormat.getDateInstance(1, jCalendar1.getLocale());
//					jCalendar1.setCalendar(calendarAct);
					Date firstDay = UtilDate.trim(new Date(jCalendar1.getCalendar().getTime().getTime()));

					int monthAnt = calendarAnt.get(Calendar.MONTH);
					int monthAct = calendarAct.get(Calendar.MONTH);

					if (monthAct != monthAnt) {
						if (monthAct == monthAnt + 2) {

							calendarAct.set(Calendar.MONTH, monthAnt + 1);
							calendarAct.set(Calendar.DAY_OF_MONTH, 1);
						}

						jCalendar1.setCalendar(calendarAct);

						BLFacade facade = MainGUI.getBusinessLogic();

						datesWithEventsCurrentMonth = facade.getEventsMonth(jCalendar1.getDate());
					}

					CreateQuestionGUI.paintDaysWithEvents(jCalendar1, datesWithEventsCurrentMonth);

					try {
						tableModelEvents.setDataVector(null, columnNamesEvents);
						tableModelEvents.setColumnCount(3); // another column added to allocate ev objects

						BLFacade facade = MainGUI.getBusinessLogic();

						Vector<domain.Event> events = facade.getEvents(firstDay);

						if (events.isEmpty())
							jLabelEvents.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NoEvents")
									+ ": " + dateformat1.format(calendarAct.getTime()));
						else
							jLabelEvents.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Events") + ": "
									+ dateformat1.format(calendarAct.getTime()));
						for (domain.Event ev : events) {
							Vector<Object> row = new Vector<Object>();

							System.out.println("Events " + ev);

							row.add(ev.getEventNumber());
							row.add(ev.getDescription());
							row.add(ev); // ev object added in order to obtain it with tableModelEvents.getValueAt(i,2)
							tableModelEvents.addRow(row);
						}
						tableEvents.getColumnModel().getColumn(0).setPreferredWidth(25);
						tableEvents.getColumnModel().getColumn(1).setPreferredWidth(268);
						tableEvents.getColumnModel().removeColumn(tableEvents.getColumnModel().getColumn(2)); // not
																												// shown
																												// in
																												// JTable
					} catch (Exception e1) {

						jLabelQueries.setText(e1.getMessage());
					}

				}
			}
		});

		this.getContentPane().add(jCalendar1, null);

		scrollPaneEvents.setBounds(new Rectangle(292, 50, 346, 150));
		scrollPaneQueries.setBounds(new Rectangle(40, 274, 406, 116));

		tableEvents.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				int i = tableEvents.getSelectedRow();
				domain.Event ev = (domain.Event) tableModelEvents.getValueAt(i, 2); // obtain ev object
				queries = ev.getQuestions();

				tableModelQueries.setColumnCount(3);
				tableModelQueries.setDataVector(null, columnNamesQueries);

				if (queries.isEmpty())
					jLabelQueries.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NoQueries") + ": "
							+ ev.getDescription());
				else
					jLabelQueries.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("SelectedEvent") + " "
							+ ev.getDescription());

				for (domain.Question q : queries) {
					
						Vector<Object> row = new Vector<Object>();

						row.add(q.getQuestionNumber());
						row.add(q.getQuestion());
						row.add(q);
						tableModelQueries.addRow(row);
					
				}
				if (tableModelQueries.getRowCount() == 0) {
					jLabelQueries.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NoQueries") + ": "
							+ ev.getDescription());
				}

				tableModelKuotak.setDataVector(null, columnNamesKuotak);
				tableQueries.getColumnModel().getColumn(0).setPreferredWidth(25);
				tableQueries.getColumnModel().getColumn(1).setPreferredWidth(268);
				// tableQueries.getColumnModel().removeColumn(tableQueries.getColumnModel().getColumn(2));
			}
		});

		tableQueries.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (mota == 1) {
					int i = tableQueries.getSelectedRow();
					// selectedQuestion = (domain.Question) tableModelQueries.getValueAt(i, 2); //
					// obtain ev object
					selectedQuestion = queries.get(i);
				}

				int i = tableQueries.getSelectedRow();
				selectedQuestion = queries.get(i);
				// domain.Question q = (domain.Question) tableModelQueries.getValueAt(i, 2); //
				// obtain ev object
				if (queries.get(i).getResult() == null) {
					Kuotak = queries.get(i).getKuotak();
				
	
					tableModelKuotak.setColumnCount(4);
					tableModelKuotak.setDataVector(null, columnNamesKuotak);
	
					for (domain.Kuota k : Kuotak) {
						Vector<Object> row = new Vector<Object>();
	
						System.out.println("Kuotak " + k);
	
						row.add(k.getKuotaZenbakia());
						row.add(k.getKuota());
						row.add(k.getErantzunP());
						row.add(k);
						tableModelKuotak.addRow(row);
					}
					tableKuotak.getColumnModel().getColumn(0).setPreferredWidth(150);
					tableKuotak.getColumnModel().getColumn(1).setPreferredWidth(50);
					tableKuotak.getColumnModel().getColumn(2).setPreferredWidth(300);
					// tableKuotak.getColumnModel().removeColumn(tableKuotak.getColumnModel().getColumn(3));
				}
			}
		});

		tableKuotak.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = tableKuotak.getSelectedRow();
				// selectedKuota = (domain.Kuota) tableModelKuotak.getValueAt(i, 3);
				selectedKuota = Kuotak.get(i);
				btnKuotakErakutsiButton.setEnabled(true);
			}
		});

		scrollPaneEvents.setViewportView(tableEvents);
		tableModelEvents = new DefaultTableModel(null, columnNamesEvents);

		tableEvents.setModel(tableModelEvents);
		tableEvents.getColumnModel().getColumn(0).setPreferredWidth(25);
		tableEvents.getColumnModel().getColumn(1).setPreferredWidth(268);

		scrollPaneQueries.setViewportView(tableQueries);
		tableModelQueries = new DefaultTableModel(null, columnNamesQueries);

		tableQueries.setModel(tableModelQueries);
		tableQueries.getColumnModel().getColumn(0).setPreferredWidth(25);
		tableQueries.getColumnModel().getColumn(1).setPreferredWidth(268);

		scrollPane.setViewportView(tableKuotak);
		tableModelKuotak = new DefaultTableModel(null, columnNamesKuotak);

		tableKuotak.setModel(tableModelKuotak);
		tableKuotak.getColumnModel().getColumn(0).setPreferredWidth(150);
		tableKuotak.getColumnModel().getColumn(1).setPreferredWidth(75);
		tableKuotak.getColumnModel().getColumn(2).setPreferredWidth(300);

		this.getContentPane().add(scrollPaneEvents, null);
		this.getContentPane().add(scrollPaneQueries, null);
		this.getContentPane().add(scrollPane, null);
		scrollPane.setBounds(501, 274, 331, 116);

		getContentPane().add(scrollPane);
		lblKuotakLabel.setBounds(501, 248, 46, 14);

		getContentPane().add(lblKuotakLabel);
		DeleteAnizkoitzButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KuotaModel.removeAllElements();
				apustuAnizkoitz.clear();
				AnizkoitzBox.setModel(KuotaModel);
			}
		});
		DeleteAnizkoitzButton.setBounds(707, 15, 140, 23);

		getContentPane().add(DeleteAnizkoitzButton);

	}

	private void jButton2_actionPerformed(ActionEvent e) {
		// this.setVisible(false);
		if (mota == 0)
			MainGUI.maingui.setVisible(true);
		else if (mota == 1)
			LangileGUI.langileaGUI.setVisible(true);
		else if (mota == 2) {
			UserGUI.usergui.setVisible(true);
			LoginGUI.logeatua = (Bezeroa) MainGUI.getBusinessLogic().updateUser(LoginGUI.logeatua.Erabiltzailea);
		}
		dispose();
	}
}
