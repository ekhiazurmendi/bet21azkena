package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.BLFacade;
import businessLogic.BLFacadeImplementation;
import domain.Bezeroa;
import domain.Langilea;
import domain.Pertsona;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Locale;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public class LoginGUI extends JFrame {
	public static Pertsona logeatua;
	public static LoginGUI loginGui;
	private JPanel contentPane;
	private JTextField textErabiltzaile;
	private JPasswordField pasahitzaField;
	private JButton btnItzuli= new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("Itzuli"));
	private JCheckBox chckbxEzNaizRobot = new JCheckBox(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NOROBOT"));
	private JLabel lblErabiltzailea = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERABILTZAILEA"));
	private JLabel lblPasahitza = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZA"));
	private JLabel lblPasahitzaAhaztu = new JLabel(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZAAHAZTU"));
	private JButton btnLOGIN = new JButton("LOGIN");
	private JButton btnNewButton = new JButton(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERREGISTRATU"));
	private JTextArea textArea = new JTextArea();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI frame = new LoginGUI();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginGUI() {
		try {
			this.jbInit();
		}catch(Exception e) {
			
		}
	}
	private void jbInit() throws Exception{
		BLFacade facade= MainGUI.getBusinessLogic();
		loginGui = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textErabiltzaile = new JTextField();
		textErabiltzaile.setBounds(136, 62, 203, 20);
		contentPane.add(textErabiltzaile);
		textErabiltzaile.setColumns(10);
		
		
		lblErabiltzailea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERABILTZAILEA")); //$NON-NLS-1$ //$NON-NLS-2$
		lblErabiltzailea.setBounds(39, 64, 100, 17);
		contentPane.add(lblErabiltzailea);
		
		
		lblPasahitza.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZA")); //$NON-NLS-1$ //$NON-NLS-2$
		lblPasahitza.setBounds(39, 99, 88, 20);
		contentPane.add(lblPasahitza);
		
		pasahitzaField = new JPasswordField();
		pasahitzaField.setBounds(136, 99, 203, 20);
		contentPane.add(pasahitzaField);
		
		
		textArea.setEditable(false);
		textArea.setBounds(118, 206, 203, 44);
		contentPane.add(textArea);
		
		
		chckbxEzNaizRobot.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NOROBOT")); //$NON-NLS-1$ //$NON-NLS-2$
		chckbxEzNaizRobot.setBounds(242, 122, 182, 20);
		contentPane.add(chckbxEzNaizRobot);
		
		
		btnLOGIN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String e= textErabiltzaile.getText();
				String p= pasahitzaField.getText();
				if(!e.isEmpty() && !p.isEmpty()) {
					if(chckbxEzNaizRobot.isSelected()) {
						Pertsona per= facade.isLogin(e,p);
						if(per==null)
							textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EZBALIOZKO"));
						else {
							logeatua=per;
							if(per instanceof Langilea) {
								loginGui.setVisible(false);
								JFrame a = new LangileGUI(per);
								a.setVisible(true);					
							}
							else if(per instanceof Bezeroa) {
								loginGui.setVisible(false);
								JFrame a = new UserGUI();
								a.setVisible(true);	
							}
						}
					}else {
						textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ROBOT"));
					}
				}else {
					textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("EZBALIOZKO"));
				}
			}
		});
		btnLOGIN.setBounds(136, 153, 112, 20);
		contentPane.add(btnLOGIN);
		
		
		
		
		lblPasahitzaAhaztu.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZAAHAZTU")); //$NON-NLS-1$ //$NON-NLS-2$
		lblPasahitzaAhaztu.setForeground(Color.BLUE.brighter());
		lblPasahitzaAhaztu.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblPasahitzaAhaztu.setBounds(67, 122, 126, 20);
		lblPasahitzaAhaztu.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
		        textArea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("APUNTATU"));
		        System.out.println(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("APUNTATU"));
		    }
		});
		
		contentPane.add(lblPasahitzaAhaztu);
		
		
		btnNewButton.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERREGISTRATU")); //$NON-NLS-1$ //$NON-NLS-2$
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame a = new RegisterGUI(); 
				a.setVisible(true);
				loginGui.setVisible(false);
			}
		});
		btnNewButton.setBounds(298, 153, 126, 20);
		contentPane.add(btnNewButton);
		
		//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-1$ //$NON-NLS-2$
		btnItzuli.setBounds(10, 175, 112, 20);
		contentPane.add(btnItzuli);
		btnItzuli.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//loginGui.setVisible(false);
				MainGUI.maingui.setVisible(true);
				dispose();
				
			}
		});
		
	}
	public void redibujar() {
		System.out.println(Locale.getDefault());
		lblErabiltzailea.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERABILTZAILEA"));
		lblPasahitza.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZA"));
		chckbxEzNaizRobot.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("NOROBOT"));
		lblPasahitzaAhaztu.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("PASAHITZAAHAZTU"));
		btnNewButton.setText(ResourceBundle.getBundle(MainGUI.hizkuntza).getString("ERREGISTRATU"));
	}
}
