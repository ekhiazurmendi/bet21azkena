package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Langilea extends Pertsona implements Serializable{
	public int Soldata;

	public Langilea() {
		super();
	}
	
	public Langilea(String dNI, String herrialdea, String izena, String abizena, Date jaiotzeData, String korreoa,
			String erabiltzailea, String pasahitza, int Soldata) {
		super(dNI, herrialdea, izena, abizena, jaiotzeData, korreoa, erabiltzailea, pasahitza);
		this.Soldata=Soldata;
	}

	public int getSoldata() {
		return Soldata;
	}

	public void setSoldata(int soldata) {
		Soldata = soldata;
	}
	
	
}



