package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlSeeAlso ({MezuGrupala.class, MezuLaguna.class})
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Mezua implements Serializable{
	@XmlID
	@XmlJavaTypeAdapter(IntegerAdapter.class)
	@Id @GeneratedValue
	private Integer mezuZenbakia;
	@XmlIDREF
	@OneToOne(fetch=FetchType.EAGER)
	private Bezeroa nork;
	@XmlIDREF
	@OneToOne(fetch=FetchType.EAGER)
	private Bezeroa nori;
	private boolean erantzunda;
	private Date data;
	private String testua;
	
	public Mezua() {
		
	}

	public Mezua(Bezeroa nork, Bezeroa nori, Date data) {
		super();
		this.setNork(nork);
		this.setNori(nori);
		this.setData(data);
		this.setErantzunda(false);
	}
	
	public Bezeroa getNork() {
		return nork;
	}

	public void setNork(Bezeroa nork) {
		this.nork = nork;
	}

	public boolean getErantzunda() {
		return erantzunda;
	}

	public void setErantzunda(boolean erantzunda) {
		this.erantzunda = erantzunda;
	}

	public Bezeroa getNori() {
		return nori;
	}

	public void setNori(Bezeroa nori) {
		this.nori = nori;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getTestua() {
		return testua;
	}

	public void setTestua(String testua) {
		this.testua = testua;
	}

	public Integer getMezuZenbakia() {
		return mezuZenbakia;
	}

	public void setMezuZenbakia(Integer mezuZenbakia) {
		this.mezuZenbakia = mezuZenbakia;
	}
	
	

}
