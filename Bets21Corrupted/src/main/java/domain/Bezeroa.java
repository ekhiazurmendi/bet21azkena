package domain;

import java.util.List;
import java.util.Vector;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlIDREF;
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Bezeroa extends Pertsona implements Serializable{
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL/*, orphanRemoval=true*/)
	private List<Apustua> apustuL=new Vector<Apustua>();
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	private List<DiruMugimendua> DiruML=new Vector<DiruMugimendua>();
	public String kontukorrontea; 
	public float dirua;
	@XmlIDREF
	@OneToMany(fetch=FetchType.EAGER)
	private List<Mezua> mezuak=new Vector<Mezua>();
	@XmlIDREF
	@OneToMany(fetch=FetchType.EAGER)
	private List<Bezeroa> lagunak=new Vector<Bezeroa>();
	@XmlIDREF
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
	private List<Kopiatu> aaab=new Vector<Kopiatu>();
	@XmlIDREF
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
	private List<Kopiatu> kopiatzaileak=new Vector<Kopiatu>();
	
	private boolean kopiatzerikBai;
	
	
	public Bezeroa() {
		super();
	}
	public Bezeroa(String dNI, String herrialdea, String izena, String abizena, Date jaiotzeData, String korreoa, String erabiltzailea, String pasahitza, String kontukorrontea, boolean kopiatzerikbai) {
		super(dNI, herrialdea, izena, abizena, jaiotzeData, korreoa, erabiltzailea, pasahitza);
		this.kontukorrontea=kontukorrontea;
		this.dirua=0;
		this.kopiatzerikBai=kopiatzerikbai;
	}
	
	
	
	public boolean isKopiatzerikBai() {
		return kopiatzerikBai;
	}
	public void setKopiatzerikBai(boolean kopiatzerikBai) {
		this.kopiatzerikBai = kopiatzerikBai;
	}
	public String getKontukorrontea() {
		return kontukorrontea;
	}
	public void setKontukorrontea(String kontukorrontea) {
		this.kontukorrontea = kontukorrontea;
	}
	
	public float getDirua() {
		return dirua;
	}
	public void setDirua(float dirua) {
		this.dirua = dirua;
	}
	public void addApustu(Apustua apustu) {
		this.apustuL.add(apustu);
	}
	public void addDiruM(DiruMugimendua diruM) {
		this.DiruML.add(diruM);
	}
	public List<Apustua> getApustuL() {
		return apustuL;
	}
	public List<DiruMugimendua> getDiruML() {
		return DiruML;
	}
	public float getEtekinak() {
		float etekin=0;
		for(DiruMugimendua a: this.DiruML) {
			etekin+=a.getZenbat();
		}
		return etekin;
	}
	public void updateDirua(float diruKop, Date data, String desk) {
		this.DiruML.add(new DiruMugimendua(data, diruKop, desk, this));
		this.dirua+=diruKop;
	}
	public void deleteApustu(Apustua apustu){
		apustuL.remove(apustu);
	}
	public Apustua addNewApustua(Apustua a) {
		apustuL.add(a);
		return a;
	}
	
	public DiruMugimendua addNewDiruMugimendua(Date data, float zenbat, String desc, Bezeroa bezero) {
		DiruMugimendua a= new DiruMugimendua(data, zenbat, desc, bezero);
		DiruML.add(a);
		return a;
	}
	
	public List<Mezua> getMezuak() {
		return mezuak;
	}
	public void setMezuak(List<Mezua> mezuak) {
		this.mezuak = mezuak;
	}
	
	public void addLaguna(Bezeroa b) {
		this.lagunak.add(b);
	}
	public List<Bezeroa> getLagunak() {
		return lagunak;
	}
	public void setLagunak(List<Bezeroa> lagunak) {
		this.lagunak = lagunak;
	}
	public List<Kopiatu> getaaab() {
		return aaab;
	}
	public void setaaab(List<Kopiatu> bbbb) {
		this.aaab = bbbb;
	}
	
	public List<Kopiatu> getKopiatzaileak() {
		return kopiatzaileak;
	}
	
	public void setKopiatzaileak(List<Kopiatu> kopiatzaileak) {
		this.kopiatzaileak = kopiatzaileak;
	}
	
	public void setApustuL(List<Apustua> apustuL) {
		this.apustuL = apustuL;
	}
	public void setDiruML(List<DiruMugimendua> diruML) {
		DiruML = diruML;
	}
	
	public void addMezua(Mezua m) {
		this.getMezuak().add(m);
	}
	
	public void addKopiatzailea(Kopiatu k) {
		kopiatzaileak.add(k);
	}
	
	public void addaaab(Kopiatu k) {
		this.aaab.add(k);
	}
	
	public List<Apustua> addApustuenKopiak(Bezeroa bbbc, float zenbat) {
		Apustua a;
		List<Bezeroa> b=new Vector<Bezeroa>();
		b.add(this);
		List<Apustua> apustuak=new Vector<Apustua>();
		float beharrezkoDirua=0;
		for(Apustua apustua:bbbc.getApustuL()) {
			if(!apustua.getAmaituta()) {
				if(apustua.getBezeroak().size()==1) {
					a=new Apustua(apustua.getBet()*zenbat, new Date(), b, apustua.getKuota());
					apustuak.add(a);
					beharrezkoDirua=beharrezkoDirua + apustua.getBet()*zenbat;
				}
			}
		}
		if(beharrezkoDirua<this.getDirua()) {
			return apustuak;
		}
		return null;
	}
	public void ezabatuMezua(Mezua m1) {
		this.mezuak.remove(m1);
		
	}
	
	public Apustua getLastApustu() {
		return this.apustuL.get(this.apustuL.size()-1);
	}
	
	public String toString() {
        return this.Erabiltzailea;

    }
	@Override
	public boolean equals(Object o) {
		return this.Erabiltzailea.equals(((Bezeroa)o).getErabiltzailea());
	}
}
