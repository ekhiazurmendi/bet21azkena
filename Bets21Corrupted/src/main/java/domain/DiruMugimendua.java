package domain;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class DiruMugimendua implements Serializable{
	@XmlID
	@XmlJavaTypeAdapter(IntegerAdapter.class)
	@Id @GeneratedValue
	private Integer id;
	private Date data;
	private float zenbat;
	private String desc;
	@XmlIDREF
	private Bezeroa bezero;
	
	public DiruMugimendua() {
		
	}
	
	public DiruMugimendua(Date data, float zenbat, String desc, Bezeroa bezero) {
		this.data = data;
		this.zenbat = zenbat;
		this.desc = desc;
		this.bezero = bezero;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public float getZenbat() {
		return zenbat;
	}
	public void setZenbat(float zenbat) {
		this.zenbat = zenbat;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Bezeroa getBezero() {
		return bezero;
	}
	public void setBezero(Bezeroa bezero) {
		this.bezero = bezero;
	}
	public String toString() {
		return this.data.toString()+" "+this.zenbat+" "+this.desc;
	}
}
