package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import domain.*;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class MezuGrupala extends Mezua implements Serializable {
	@XmlIDREF
	@OneToMany(fetch = FetchType.EAGER)
	private Apustua apustua;

	public MezuGrupala() {
		super();
	}

	public MezuGrupala(Bezeroa nork, Bezeroa nori, Date data) {
		super(nork, nori, data);
		this.setTestua(data.toString() + ": " + nork + " hasitako apustu grupalean parte hartu nahi duzu?");
		// this.mezuZenbakiaGrupalaaaaaa=
		// nork.toString()+data.toString()+nori.toString();
	}

	public Apustua getApustua() {
		return apustua;
	}

	public void setApustua(Apustua apustua) {
		this.apustua = apustua;
	}
	/*
	 * public String getmezuZenbakiaGrupalaaaaaa() { return
	 * this.mezuZenbakiaGrupalaaaaaa; }
	 * 
	 * public void setmezuZenbakiaGrupalaaaaaa(String mezuZenbakiaGrupala) {
	 * this.mezuZenbakiaGrupalaaaaaa = mezuZenbakiaGrupala; }
	 */

}
