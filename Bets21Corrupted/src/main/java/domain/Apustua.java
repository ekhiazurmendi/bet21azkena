package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Apustua implements Serializable{
	@XmlID
	@XmlJavaTypeAdapter(IntegerAdapter.class)
	@Id @GeneratedValue
	private Integer id;
	private float bet;
	private Date data;
	@XmlIDREF
	@OneToMany(fetch=FetchType.EAGER)
	private List<Bezeroa> bezeroak=new Vector<Bezeroa>();
	@OneToMany(fetch=FetchType.EAGER)
	private List<Kuota> kuota=new Vector<Kuota>();
	private boolean amaituta;
	@XmlIDREF
	@OneToMany(fetch=FetchType.EAGER)
	private List<MezuGrupala> MezuGrupalak=new Vector<MezuGrupala>();
	private Boolean kopiatua;
	private int kont;
	
	public Apustua() {
		super();
	}
	
	public Apustua(float bet, Date data, List<Bezeroa> bezero, List<Kuota> kuotak) {
		this.bet = bet;
		this.data = data;
		this.bezeroak = bezero;
		this.kuota=kuotak;
		this.amaituta=false;
		this.kont=0;
		this.kopiatua=null;
	}
	public Apustua(float bet, Date data, List<Bezeroa> bezero, List<Kuota> kuotak, Boolean kopiatua) {
		this.bet = bet;
		this.data = data;
		this.bezeroak = bezero;
		this.kuota=kuotak;
		this.amaituta=false;
		this.kont=0;
		this.kopiatua=kopiatua;
	}
	
	public List<Bezeroa> getBezeroak() {
		return bezeroak;
	}

	public void setBezeroak(List<Bezeroa> bezeroak) {
		this.bezeroak = bezeroak;
	}

	public List<MezuGrupala> getMezuGrupalak() {
		return MezuGrupalak;
	}

	public void setMezuGrupalak(List<MezuGrupala> mezuGrupalak) {
		MezuGrupalak = mezuGrupalak;
	}

	public int getKont() {
		return kont;
	}

	public void setKont() {
		this.kont = kont +1;
	}

	public float getBet() {
		return bet;
	}
	public void setBet(float bet) {
		this.bet = bet;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public List<Kuota> getKuota() {
		return kuota;
	}
	public void setKuota(List<Kuota> kuota) {
		this.kuota = kuota;
	}
	public void apustuaezabatu() {
		if(!this.amaituta) {
			for(Bezeroa bezero: this.bezeroak) {
				bezero.updateDirua(bet, new Date(), "Apustua ezabatu");
			}
		}
		for(Bezeroa bezero: this.bezeroak) {
			bezero.getApustuL().remove(this);
		}
		for(Kuota kuota: this.kuota) {
			kuota.getApustuL().remove(this);
		}
	}
	public int getId() {
		return id;
	}
	/*
	public String toString() {
		return "Zenbat: "+bet+" /Data: "+data+" /Galderaren deskripzioa: "+kuota.getGaldera().getQuestion();
	}
	*/
	public boolean getAmaituta() {
		return amaituta;
	}
	public void setAmaituta(boolean amaituta) {
		this.amaituta = amaituta;
	}

	public List<MezuGrupala> getMezuGrupala() {
		return MezuGrupalak;
	}

	public void setMezuGrupala(List<MezuGrupala> mezuGrupalak) {
		MezuGrupalak = mezuGrupalak;
	}
	
	public void addBezeroa(Bezeroa b) {
		this.getBezeroak().add(b);
	}

	public Boolean getKopiatua() {
		return kopiatua;
	}

	public void setKopiatua(Boolean kopiatua) {
		this.kopiatua = kopiatua;
	}
}
