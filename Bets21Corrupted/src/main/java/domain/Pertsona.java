package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSeeAlso;
@XmlSeeAlso ({Bezeroa.class, Langilea.class})
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Pertsona implements Serializable{
	public String DNI;
	public String Herrialdea;
	public String Izena;
	public String Abizena;
	public Date JaiotzeData;
	public String Korreoa;
	@Id
	@XmlID
	public String Erabiltzailea;
	public String Pasahitza;

	public Pertsona(String dNI, String herrialdea, String izena, String abizena, Date jaiotzeData, String korreoa,
			String erabiltzailea, String pasahitza) {
		super();
		DNI = dNI;
		Herrialdea = herrialdea;
		Izena = izena;
		Abizena = abizena;
		JaiotzeData = jaiotzeData;
		Korreoa = korreoa;
		Erabiltzailea = erabiltzailea;
		this.Pasahitza = pasahitza;
	}
	public Pertsona() {
		
	}
	/*
	public String getDNI() {
		return DNI;
	}
	*/
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	public String getHerrialdea() {
		return Herrialdea;
	}
	public void setHerrialdea(String herrialdea) {
		Herrialdea = herrialdea;
	}
	public String getIzena() {
		return Izena;
	}
	public void setIzena(String izena) {
		Izena = izena;
	}
	public String getAbizena() {
		return Abizena;
	}
	public void setAbizena(String abizena) {
		Abizena = abizena;
	}
	public Date getJaiotzeData() {
		return JaiotzeData;
	}
	public void setJaiotzeData(Date jaiotzeData) {
		JaiotzeData = jaiotzeData;
	}
	public String getKorreoa() {
		return Korreoa;
	}
	public void setKorreoa(String korreoa) {
		Korreoa = korreoa;
	}
	public String getErabiltzailea() {
		return Erabiltzailea;
	}
	public void setErabiltzailea(String erabiltzailea) {
		Erabiltzailea = erabiltzailea;
	}
	public String getPasahitza() {
		return Pasahitza;
	}
	public void setPasahitza(String pasahitza) {
		this.Pasahitza = pasahitza;
	}
	
}

