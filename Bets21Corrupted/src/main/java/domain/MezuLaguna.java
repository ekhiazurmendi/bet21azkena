package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class MezuLaguna extends Mezua implements Serializable{

	public MezuLaguna() {
		// TODO Auto-generated constructor stub
	}

	public MezuLaguna(Bezeroa nork, Bezeroa nori, Date data) {
		super(nork, nori, data);
		setTestua(nork+" erabiltzailearen laguna izan nahi duzu?");
	}
}
